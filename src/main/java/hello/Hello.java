package hello;

public class Hello {

    private Hello() {}
    private static final String HELLO = "hello.Hello";
    public static String giveHello() {
        return HELLO;
    }
}
